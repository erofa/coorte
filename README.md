# COORTE

CoOrtE (Correcteur et Convertisseur en Orthographe d'EROFA) est une suite logicielle qui fournit des outils orthographiques applicant la proposition d'[EROFA](http://erofa.free.fr/) de réforme de l'orthographe du français. Elle comporte actuellement :

 * Un **convertisseur**, sous trois formes :
   * convertisseur de texte brut
   * convertisseur de fichiers (PDF, Word, epub,...)
   * extension de navigateur (conversion automatique des pages web)
 * Un **correcteur**

## DOR

Le DOR est le *Dictionnaire de l’Orthographe Rationalisée du Français*, rédigé et maintenu par l'association EROFA.
Nous l'avons convertit en table csv que nous mettons à jour régulièrement. Cette table a été utilisée pour concevoir le correcteur et le convertisseur.

## Convertisseur

Le site web [Orthographe Rationnelle](https://orthographe-rationnelle.info/) donne accès au convertisseur de texte brut en ligne. Il propose aussi la conversion de fichiers (PDF, Word, epub,...).

La conversion automatique de pages web est disponible en installant l'extension associée.
 * [Firefox](https://addons.mozilla.org/fr/firefox/addon/orthographe-rationnelle/)
 * [Chrome](https://chromewebstore.google.com/detail/orthographe-rationnelle/jdicbfmgcajnpealjodkghahiakdafcl)
 * Safari (en cours)

## Correcteur

Le correcteur est une extension de suggestion de correction orthographique. Il est disponible pour :
 * [Thunderbird](https://addons.thunderbird.net/fr/thunderbird/addon/corecteur-ortografe-simpl-thun/)
 * [Firefox](https://addons.mozilla.org/fr/firefox/addon/corecteur-ortografe-simplifiee/)
 * LibreOffice (en cours)



## Licences

Le correcteur est sous licence [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html).
La version modifiée de Lexique 3.8 est sous licence [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/). La version modifiée du Witkionnaire est sous licence [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).
Le reste du convertisseur est sous license [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html).
La partie d'évaluation est sous licence [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.fr).

La licence du DOR est en cours de détermination.


## Publication

Pour citer Coorte, merci de citer l'article suivant :

> Richard, V. D., Fruchard, E. & Gatien-Baron, V. (2024). COORTE : Une trousse d'outils pour mettre en pratique une proposition de réforme de l'orthographe française. À paraitre dans 9e Congrès Mondial de Linguistique Française, Lausanne.

# Versions

Voir chaque dossier.

## Crédit

Emmanuel Fruchard, Valentin Gatien-Baron, Valentin D. Richard

Pour toute question, remarque ou suggestion, nous contacter à `contact@erofa.org`