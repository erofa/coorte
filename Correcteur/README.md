# Correcteur

Le correcteur est une extension de suggestion de correction orthographique. Il est disponible pour :
 * [Thunderbird](https://addons.thunderbird.net/fr/thunderbird/addon/corecteur-ortografe-simpl-thun/)
 * [Firefox](https://addons.mozilla.org/fr/firefox/addon/corecteur-ortografe-simplifiee/)
 * LibreOffice (en cours)

## Fonctionnement

Le correcteur utilise le programme hunspell, qui a deux fichiers de paramétrage :
 * Un lexique qui contient des lemmes `fr-erofa.dic`
 * Des règles de flexion (par ex. pour déduire du lexique les formes au pluriel, au féminin et les conjugaisons) `fr-erofa.aff`
  
Ces deux fichiers peuvent être récupérés en ouvrant le fichier `.xpi` comme une archive (par ex. en le renommant en `.zip`).

## Conception

Les détails de la conception de l'outil sont disponible dans le fichier `Conception_correcteur_coorte.pdf`.

## Versions

 * `v1.0.0` : version de publication
 * `cmlf2024`: version d'évaluation du correcteur pour l'article du CMLF 2024