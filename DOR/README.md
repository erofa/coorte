# DOR

Le DOR est le *Dictionnaire de l’Orthographe Rationalisée du Français*, rédigé et maintenu par l'association EROFA. La version PDF est librement accessible sur le [site de l'éditeur](http://www.lambert-lucas.com/wp-content/uploads/2022/11/OA-dictionnaire-EROFA.pdf).

> Gruaz, C. (Éd.). (2018). [Dictionnaire de l’orthographe rationalisée du français](http://www.lambert-lucas.com/livre/dictionnaire-de-lorthographe-rationalisee-du-francais/). Éditions Lambert-Lucas.

Nous avons converti le fichier PDF en table csv que nous mettons à jour régulièrement.

## Comptes rendus

Les fichiers `DOR_Propositions-et-décisions` contiennent les propositions de changement du DOR ainsi que les décisions qui ont été prises lors d'une réunion du bureau d'Érofa.

Légende :
 * Jaune : En discussion
 * Jaune très pâle : Si le temps le permet
 * Blanc : Proposition déposé à l'ordre du jour
 * Vert : Proposition appouvée
 * Gris : Proposition Rejetée

## Versions

 * 1.1.2, février 2025 : ajout de 36 adverbes en -emment avec [e] prononcé /a/ avec remplacement de [emm] par [am] (apparemment > aparament). De manière similaire : solennel > solanel et couenne > couane. Suppression du remplacement de [c] par [ss] dans doucettement > doucètement au lieu de doussètement (idem doucereux et doucette). Tout ceci suivant la décision prise en réunion le 6 février 2025. 
 * 1.1.1, mai 2024 : ajout de 59 entrées sur des verbes conjugués et ajout d'une colonne "complément" dans laquelle sont reportées les indications telles que "(se)" pour les verbes pronominaux, suivant les décisions prises à la réunion du 22 avril 2024
 * 1.1.0, février 2024 : ajout d'une centaine d'entrées sur les -x finaux, suivant les décision prises à la réunion du 14 février 2024
 * 1.0.0, juillet 2023 : portage depuis la version papier publiée en 2018