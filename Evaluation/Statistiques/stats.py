#! /usr/bin/env python3

import csv
import numpy as np
import sys

USAGE = """python3 stats.py [--lexique]
Procède à des statistiques sur le DOR, en calculant les fréqences à partir
d'un corpus (GLÀFF par défaut)

OPTIONS:
    --lexique   Sélectionne Lexique comme lexique de calcul de fréquences"""

##### Statistiques sur le dictionnaire de proposition
# de réforme de l'orthographe d'EROFA

# ##### Constantes
dir = "/home/vrichard/Documents/Projects/FonoFlech/src/data/lex/"
# MANULEX
manulex_filename = dir + "manulex.csv"
nb_line_manulex = 43398
total_freq_manulex = 775583.5900000675
# Lexique
lexique_filename = dir + "lexique.csv"
nb_line_lexique  = 142694
total_freq_lexique = 907213.0900003504
# GLÀFF
glaff_filename = dir + "glaff.csv"
nb_line_glaff = 1250000
total_freq_glaff = 2656343.2634227714
# DOR
DOR_filename = "DOR-numérique_v1.0.csv"

# pos_dic = {
#     "ADJ"     : "adj.",
#     "ADJ:dem" : "adj.dém.",
#     "ADJ:ind" : "adj.",
#     "ADJ:int" : "adj.interr.",
#     "ADJ:num" : "adj.num.",
#     "ADJ:pos" : "adj.",
#     "ADV"     : "adv.",
#     "ART:def" : "art.",  # non présent dans le dctionnaire
#     "ART:ind" : "art.",
#     "AUX"     : "v.",
#     "CON"     : "conj.",
#     "LIA"     : "", # ignored
#     "NOM"     : "n.",
#     "ONO"     : "interj.",
#     "PRE"     : "prép.",
#     "PRO:dem" : "pron.dém.",
#     "PRO:ind" : "pron.",
#     "PRO:int" : "pron.interr.",
#     "PRO:per" : "pron.pers.",
#     "PRO:pos" : "pron.poss.",
#     "PRO:rel" : "pron.rel.",
#     "VER"     : "v."
# }
# nombre_dic = {"p":"pl.", "s":"sg."}



# ##### Fonctions auxiliaires

# def pos_convert(cgram:str, genre:str, nombre:str) -> str:
#     """"Transforme la catégories grammaticale du format Lexique au format
#     Dictionnaire d'EROFA"""
#     # pb. adj.m.sg.pl.
#     return pos_dic[cgram] + genre + "." + nombre_dic[nombre]

def proj_freqfilm(entry):
    assert len(entry) == 2
    return entry[1][0]



##### Récupération du lexique en tant que dictionnaire python
# clé : forme graphique
# valeur : couple (freqfilms, freqlivres)

# Opening asked file
if len(sys.argv) > 1:
    if sys.argv[1] == "--help" or sys.argv[1] == "-h":
        print(USAGE)
    elif sys.argv[1] == "--lexique":
        File = open(lexique_filename, 'r')
        total_freq = total_freq_lexique
        nb_line = nb_line_lexique
    elif sys.argv[1] == "--manulex":
        File = open(manulex_filename, 'r')
        total_freq = total_freq_manulex
        nb_line = nb_line_manulex
else: # GLÀFF par défaut
    File = open(glaff_filename)
    total_freq = total_freq_glaff
    nb_line = nb_line_glaff

lexique = csv.reader(File, delimiter=",")
lex = {}

for line in lexique:
    # Essai 1: juste avec la forme graphique comme clé
    tokens = line[0]
    if tokens not in lex.keys():
        lex[tokens] = float(line[3])
    else:
        lex[tokens] = max(lex[tokens], float(line[3]))

File.close()


##### Calcul des statistiques sur le Dictionnaire
# dict filtered : [word, (freqfilm, freqlivre), reason_list]
dict_filtered = []

nb_line_dict_nonfiltered = 0
count_reason_nonfiltered = {"*":0, "1":0, "2":0, "3":0, "3’":0}


Missing = open("mots_absents.txt","w")
Dict = open(DOR_filename, 'r', newline='')
dict = csv.reader(Dict, delimiter=",")
header = True

for line in dict:
    if header:
        header = False
        continue

    # A. non filtré
    nb_line_dict_nonfiltered += 1
    # retirer la potentielle parenthèse
    tokens = line[1].split(" ")
    mot = tokens[0]  # ex. "fritter (se)"
    if len(tokens) >= 2:
        if len(tokens[1]) > 0 and tokens[1][0] != "(": # ex. "a capella"
            mot = " ".join(tokens)

    # Raison
    reasons = line[4].split(' ')
    for reason in reasons:
        if len(reason) > 0:
            count_reason_nonfiltered[reason] += 1

    # B. filtré selon Lexique
    if mot not in lex.keys():
        Missing.write(mot+"\n")
    else:
        # Raison
        dict_filtered.append([mot, lex[mot], reasons])

Missing.close()
Dict.close()


##### Printing Results

print("\nQuantité de mots réformés :")

print("\nFiltré selon ceux présent dans le lexique :")
print("Dans le dictionnaire : ", len(dict_filtered))
freq = sum([entry[1] for entry in dict_filtered])
print("Pondérée: ",  freq)
print("Nombre de mots affectées par raison :")
for reason in count_reason_nonfiltered.keys():
    words_of_reason = [[entry[0], entry[1]] for entry in dict_filtered \
                        if reason in entry[2]]
    words_of_reason.sort(key=lambda entry: entry[1], reverse=True)
    freq_of_reason = sum([entry[1] for entry in words_of_reason])
    most_commons = [entry[0] for entry in words_of_reason[0:10]]
    print("Raison ", reason, " : ", len(words_of_reason))
    print("\tRaison pondérée : ", freq_of_reason)
    print("\tMots affectésles plus fréquents : ", most_commons)

print("\nSans filtre (tout) :")
print("Dans le dictionnaire : ", nb_line_dict_nonfiltered)
print("Nombre de mots affectées par raison :")
for reason in count_reason_nonfiltered.keys():
    print("Raison ", reason, " : ", count_reason_nonfiltered[reason])