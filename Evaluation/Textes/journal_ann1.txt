Interco Alain Joyandet en arbitre dans le duel Chrétien-Wadoux « Construisons l'agglo en dehors des élections »

Interview La loi promulguée le 17 décembre dernier ofre désormais la possibilité de transformer la comunauté de comunes de l'aglomération de Vesoul (CCAV) en comunauté d'aglomération, un véritable changement de statut ouvrant de nouvèles perspectives de dévelopement et qu'Alain Joyandet, alors sénateur, avait vainement tenté de faire adopter par amendement en 1999.

Parmi les dix-neuf comunes composant la CCAV, seule Noidans-lès-Vesoul tenue par Jean-Pierre Wadoux s'est oposée à cète perspective. Un refus sur la forme, « on nous demande d'avancer à marche forcée sans concertation », mais pas sur le fond. En filigrane, le probable duel Chrétien-Wadoux aux prochaines élections cantonales sur Vesoul ouest (notre édition d'hier).

La CCAV ne s'est pas transformée au 1er janvier 2011, à qui la faute ? Ça ne peut pas se faire, voilà tout. Je ne veux pas entrer dans la polémique. Il faut maintenant profiter de 2011 pour préparer les choses dans de bones conditions. L'enjeu est tel qu'on ne peut pas résumer cet évènement à un problème d'élection cantonale. Je suggère qu'on laisse passer cette échéance afin d'avoir un débat serein.

D'autant que tout le monde semble d'acord sur le fond…

Je constate toutefois un désacord. On peut comprendre une accélération (par le président de la CCAV Alain Chrétien) de la procédure pour des questions financières (afin d'obtenir des crédits d'État suplémentaires, NDLR), mais en même temps, on peut comprendre que certains veulent prendre le temps du débat (Jean-Pierre Wadoux). Je pense qu'il faut une vraie discussion afin de poser, après la création du district en 1969 et l'instauration de la taxe professionèle unique (TPU) en 2001, l'acte 3 de notre intercomunalité, de la construire en dehors des élections et de la campagne électorale.

La TPU a permis d'harmoniser la fiscalité des entreprises, est-il envisageable d'appliquer la même recète aus ménages ?

C'est une vraie question. Il faut en parler dans l'esprit d'amenuiser les diférences. En fait, il faut s'intéroger sur ce que la comunauté d'aglo peut prendre comme nouvèles compétences, des compétences qui soient logiques par raport à la façon dont nous vivons ensemble dans l'intercomunalité.
