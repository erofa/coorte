POUR MOI SEULE


Sur le toit de tuiles rousses que je vois de ma fenêtre, une fumée
voudrait monter, que rabat le grand vent. Èle bouillone au sortir de
la cheminée noire come un jet d’eau sans force; èle se couche et
s’échevèle. En la regardant, je pense à beaucoup de choses que je ne
saurais pas bien dire. Certes, j’ai de l’instruction. A Paris, j’ai
suivi des cours. Je lis quelquefois. Et l’on m’a toujours afirmé que je
fais bien les lètres. Mais il est dificile de conaitre ce que l’on
éprouve et de l’exprimer exactement.

Je voudrais cependant m’y apliquer. Les journées sont longues et ma
seur Guicharde me décharge de tout le soin de la maison. En ce moment
(c’est aujourd’ui samedi), èle s’ocupe en bas à changer le papier
bleu sur les planches du bufet. Èle est prompte dans ses gestes, et
les vaissèles déplacées font en se heurtant un tapage qui inquièterait
bien mon mari, plus ménager que moi-même, et qui devrait peut-être
m’émouvoir.

Seule dans ma chambre, devant ce papier que je viens de prendre, je me
trouve toute sote, come on dit ici. Et qu’est-ce que je vais raconter,
puisqu’il ne s’est rien passé qui ne fût au dedans de moi? Cependant, je
voudrais essayer... Ce sera bien ordinaire sans doute, et tourné
maladroitement, mais persone n’en poura rire et le feu seul conaitra
ces pages, quadrillées de bleu, après que mon écriture les aura
couvertes.

... Notre maison est sombre et froide avec un seul étage et de très
grands greniers. Point de jardin. Une cour seulement, par dérière, nous
sépare de la chapèle désafectée d’un ancien couvent; un acacia maigre
y puise un peu de vie. Ses branches balancées touchent à nos fenêtres et
s’alongent de l’autre côté jusqu’aus petits vitraus jaunes et bleus;
ses fleurs, flétries presque en naissant mais cependant odorantes,
recouvrent au printemps avec la même abondance notre toit aus fortes
lucarnes et le toit ovale que surmontent encore la cloche et la crois.
Pas de vue de ce côté et pas de vue sur la rue, qui est étroite. Èle
s’apèle la rue des Massacres en souvenir d’oribles choses qui
s’acomplirent là pendant les guères de religion... Mais ce n’est pas
ainsi que je dois comencer.

Il y a cinq ans que je suis venue dans cète vile, il y en a quatre que
je suis mariée et que j’abite cète maison. Les premiers jours...

Ah! ce n’est point encore cela. Vais-je enfin y parvenir? Tout à l’eure
ils m’apèleront pour le souper et je n’aurai pas écrit quatre lignes.
Il me faudrait les premières frases; le reste sera bien facile... Cète
fois, j’ai trouvé; voici qui est vraiment pour moi le comencement de
tout:

Je me souviendrai ma vie entière du jour où maman nous raconta son
istoire.