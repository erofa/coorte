Interco Alain Joyandet en arbitre dans le duel Chrétien-Wadoux « Construisons 1 en dehors des élections »

Interview La loi promulguée le 17 décembre dernier 1 désormais la possibilité de transformer la 1 de 1 de 1 de Vesoul (CCAV) en 1 1, un véritable changement de statut ouvrant de 1 perspectives de 1 et qu'Alain Joyandet, alors sénateur, avait vainement tenté de faire adopter par amendement en 1999.

Parmi les dix-neuf 1 composant la CCAV, seule Noidans-lès-Vesoul tenue par Jean-Pierre Wadoux s'est 1 à 1 perspective. Un refus sur la forme, « on nous demande d'avancer à marche forcée sans concertation », mais pas sur le fond. En filigrane, le probable duel Chrétien-Wadoux 1 prochaines élections cantonales sur Vesoul ouest (notre édition 4).

La CCAV ne s'est pas transformée au 1er janvier 2011, à qui la faute ? Ça ne peut pas se faire, voilà tout. Je ne 1 pas entrer dans la polémique. Il faut maintenant profiter de 2011 pour préparer les choses dans de 1 conditions. L'enjeu est tel qu'on ne peut pas résumer cet 1 à un problème d'élection cantonale. Je suggère qu'on laisse passer 1 échéance afin d'avoir un débat serein.

D'autant que tout le monde semble 1 sur le fond…

Je constate toutefois un 1. On peut comprendre une accélération (par le président de la CCAV Alain Chrétien) de la procédure pour des questions financières (afin d'obtenir des crédits d'État 1, NDLR), mais en même temps, on peut comprendre que certains veulent prendre le temps du débat (Jean-Pierre Wadoux). Je pense qu'il faut une vraie discussion afin de poser, après la création du district en 1969 et l'instauration de la taxe # unique (TPU) en 2001, l'acte 3 de notre #, de la construire en dehors des élections et de la campagne électorale.

La TPU a permis 1 la fiscalité des entreprises, est-il envisageable 1 la même 1 1 ménages ?

C'est une vraie question. Il faut en parler dans l'esprit d'amenuiser les 1. En fait, il faut # sur ce que la 1 1 peut prendre 1 1 compétences, des compétences qui soient logiques par 1 à la façon dont nous vivons ensemble dans #.
