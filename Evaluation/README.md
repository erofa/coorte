# Évaluation de Coorte

Le détail de l'évaluation est à retrouver dans l'article de référence du projet.

## Textes

Le corpus d'évaluation est consitué des textes écrits en orthographe traditionnelle (pré-1990) `journal_orig.txt`, `roman_orig.txt` et `wiki_orig.txt`.

## Annotations

Trois annotateurs humains (membres de l'association EROFA) ont participé à la tâche consistant à annotater chaque mot selon le schéma suivant :

 * 0 : le mot est conforme à la norme EROFA
 * 1 : le mot n'est pas conforme à la norme EROFA
   * dans ce cas, quelle est la graphie conforme ?

Cette tâche comporte donc une couche de classification binaire et une couche de réécriture mot à mot.

## Prédiction

Trois programmes sont lancés sur le corpus d'évaluation : le correcteur (`corr`), le convertisseur (`extweb`) et ChatGPT 3.5 (`chatgpt`). Ils ont produit un texte en sortie.

Les promts et réponses complètes qui ont permis de générer les textes modifiés grâce à ChatGPT sont disponibles dans `chtgpt_conv.md`. Le prompt initial décrit la liste des règles à appliquer, donne des exemples pour chacune, et donne un exemple de texte complet réécrit. Nous avons observé que ChatGPT semblait moins bien "appliquer" les règles au fur et à mesure de la réécriture des différents textes. C'est pourquoi nous avons choisit de sélectionner les résultats de textes donnés en deuxième prompt : `journal_chatgpt` est tiré de la conversation 1, `roman_chatgpt` de la 2 et `wiki_chatgpt` de la 3.

Pour le correcteur, la première suggestion est prise (ou le token `#` si pas de suggestion). Une version avec le rang de la suggestion correcte (`corr_rank`) est aussi calculée.

## Calcul des scores

Le script de calcul des scores est `script.ipynb`.

Les textes sont tokenizés grossièrement au niveau des espaces. Sur la tâche de classification binaire, on calcule l'exactitude, la précision, le rappel et la F-mesure. Sur la tâche de réécriture, on calcule le score BLEU à l'instar d'une tâche de traduction automatique. Sur le correcteur, on calcule aussi le rang réciproque moyen (MRR).

## Statistiques

Le dossier `Statistiques` contient le script de calcul des statistiques sur le DOR.