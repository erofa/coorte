## Convertisseur

Le site web [Orthographe Rationnelle](https://orthographe-rationnelle.info/) donne accès au convertisseur de texte brut en ligne. Il propose aussi la conversion de fichiers (PDF, Word, epub,...).

La conversion automatique de pages web est disponible en installant l'extension associée.
 * [Firefox](https://addons.mozilla.org/fr/firefox/addon/orthographe-rationnelle/)
 * [Chrome](https://chromewebstore.google.com/detail/orthographe-rationnelle/jdicbfmgcajnpealjodkghahiakdafcl)
 * Safari (en cours)

Le répertoire du code source de l'extension web et du site Orthographe Rationnelle est :
> https://github.com/v-gb/ortografe

La version d'évaluation pour l'article CMLF 2024 peut être récupérée au tag `cmlf2024`.