# Lexique de conversion 1990

La liste à deux entrées montre les changements de norme orthographique des rectifications de 1990.

## Description

Les colonnes sont :
 1. Lemme du mot en orthographe classique
 2. Flexion du mot en orthographe classique
 3. Flexion en orthographe post-1990
 4. Lemme en orthographe post-1990
 5. Distance d'édition entre la flexion classique et la flexion post-1990
 6. Raison du changement :

Les numéros associés à une raison de modification sont :

 * 0: Accent grave ou aigu sur le `e` changé ou ajouté, ex. *lib**e**ro* -> *lib**é**ro*, *si**é**gerons* -> *si**è**gerons*, _cano**ë**_ -> _cano**é**_
 * 1: Accent circonflexe sur le `u` ou le `i` enlevé, ex. *go**û**t* -> *go**u**t*, *cro**î**tre* -> *cro**i**tre*
 * 2: Régularisation de certains `y` en `i`, ex. _pap**y**_ -> _pap**i**_
 * 3: Consonne double (en `b,c,d,f,m,n,r,s` ou `t`) supprimée ou rajoutée (pour cohérence dans la famille de mots), ex. *trimba**ll**er* -> *trimba**l**ler*, *persi**f**ler* -> *persi**ff**ler*
 * 4: Graphème comprenant un `h` simplififé, ex. _**h**indou_ -> *indou*, *nénu**ph**ar* -> *nénu**f**ar*
 * 5: Simplification du `k` (ou `kh`) en `c`, ex. *sans**k**rit* -> *sans**c**rit*, _**kh**alife_ -> _**c**alife_
 * 6: Suppression de l'apostrophe intra-mot, ex. *ch\'timi* -> *chtimi*, *entr\'apercevoir* -> *entrapercevoir*
 * 7: Suppression du tiret, ex. *plate-forme* -> *plateforme*
 * 8: Replacement, ajout ou suppression des trémas, ex. _ambigu**ë**_ -> _ambig**ü**e_, _**ï**ambique_ -> _**i**ambique_
 * 9: Autre : 
   * Simplification du `illi` en `ill`, ex. *serp**illi**ère* -> *serp**ill**ère*
   * Pluriel régularisé, ex. _maxim**a**_ -> _maxim**ums**_
   * Emprunt régularisé, ex. _ya**ck**_ -> _ya**k**_
   * Correction lexicale, ex. *o**i**gnon* -> *ognon*
   * Suppression des ligatures, ex. *c**œ**liaque* -> *c**é**liaque*

Quand il y a plusieurs raisons, les numéros sont séparés par un point-virgule.

Seuls les mots ayant une orthographe post-1990 différente de leur graphie classique sont présent.

## Conception

La table de conversion a été créée à partir de l'analyse des différences entre les lexiques de [Grammalecte](https://grammalecte.net/) `fr-classique` et `fr-reforme1990`. Des compléments de diverses sources sont présents en fin de liste après "zygopetalums". 

Plus précisément, la méthode utilisée exécute les étapes suivantes :
 * Évaluer toutes les flexions (féminin/masculin, singulier/ pluriel et conjugaisons) du lexique classique (451 380 flexions)
 * Évaluer toutes les flexions du lexique réformé (455 270 flexions)
 * Identifier les flexions classiques qui sont absentes de la liste des flexions réformées
 * Connecter les lemmes et les règles classiques -> réformés
 * À partir de ces tables de connexion, identifier la flexion réformée de la sous-liste des flexions classiques qui ont disparu après 1990

## Voir aussi

Une autre table de conversion classique / post-1990 a été conçue en passant [Lexique 3.83](http://www.lexique.org/) dans le logiciel [RectoVerso](https://uclouvain.be/recto-verso/essaie-recto.html) :

> https://github.com/v-gb/ortografe/blob/main/data/lexique/1990.csv

Il est utilisé pour le convertisseur classique / post-1990.